'''
Given a string we have to check whether the given String is 
valid roman numeral or not .
if valid print trrue else false
'''

import re 

def checkValidRoman(string):
    p=bool(re.search(r"^M{0,3}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$",string))
    return p

def main():
    print("Enter the number of strings")
    n=int(input(">"))
    while n>0:
        print("Enter the string")
        string=input(">")
        if(checkValidRoman(string)):
            print("It is valid")
        else:
            print("It is not valid")
        n=n-1

if __name__=='__main__':
    main()
    
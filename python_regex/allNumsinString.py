'''
Program to check all numbers in a string using regular
expression in Python
'''

import re

def getNumber(str):
    array=re.findall(r'[0-9]+',str)
    return array

def main():
    print("Enter the number of strings")
    n=int(input('>'))
    while n>0:
        print("Enter the string")
        str=input(">")
        array=getNumber(str)
        print(*array)
        n=n-1

if __name__=='__main__':
    main()
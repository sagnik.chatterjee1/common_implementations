/**
    Implementing an demo class that extends the java.lang.Thread class
    overrides the run() method available in Thread class.
    invokes thread class
 */

 import java.util.*;

 class Multithreading1Demo extends Thread{
     public void run(){
         try{
             //display thread that is running 
             System.out.println("Thread"+
             Thread.currentThread().getId()+
             "is running");
         }
         catch(Exception e)
         {
             System.out.println(" Some Exception");
             System.out.println(e.getStackTrace());
         }
     }
 }

 public class Multithreading1{
     public static void main(String[] args)
     {
         Scanner sc =new Scanner(System.in);
         System.out.println("Enter the number of threads ");
         int n=sc.nextInt();
         for(int i=0;i<n;i++)
         {
             Multithreading1Demo obj =new Multithreading1Demo();
             obj.start();
         }
     }
 }
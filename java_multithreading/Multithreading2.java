/**
    thread creation by implementing 
    the runnable interface
 */
import java.util.*;

class Multithreading2Demo implements Runnable{
    public void run()
    {
        try{
            //Display the thread that is running 
            System.out.println("Thread"+
            Thread.currentThread().getId()+" is running");

        }
        catch(Exception e)
        {
            System.out.println("Some exception is caught");
            System.out.println(e.getStackTrace());
        }
    }
}
class Multithreading2 {
    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the number of threads ");
        int n=sc.nextInt();
        for(int i=0;i<n;i++)
        {
            Thread obj =new Thread(new Multithreading2Demo());
            obj.start();
        }
    }
}
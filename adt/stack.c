/***
 * A stack implementation written in C
 * Uses an array to store the data 
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <limits.h>

struct Stack{
    int top;
    unsigned capacity;
    int *array;
};

struct Stack* createStack(unsigned capacity)
{
    //cretaes the stack of the required capcaity
    struct Stack*  stack =(struct Stack*)malloc(sizeof(struct Stack));

    stack->capacity=capacity;
    stack->top=-1;
    stack->array=(int*)malloc(sizeof(int)*capacity);
    return stack;
}
bool isFull(struct Stack* stack)
{
     if(stack->top== stack->capacity-1)
     return true;
    return false;
}

bool isEmpty(struct Stack* stack)
{
    if(stack->top==-1)
    return true;
return false;
}
//function to add an item to the stack
void push(struct Stack* stack,int item)
{
    if(isFull(stack))
    {
        return ;
    }
    stack->array[++stack->top]=item;
    printf("%d is pushed to stack \n",item);
}

//function to remove an item from the stack

int pop(struct Stack* stack)
{
    if(isEmpty(stack))
    {//cause nothing is there to pop
        return INT_MIN;
    }
    return stack->array[stack->top];
}

int peek(struct Stack* stack)
{
    if(isEmpty(stack))
    return INT_MIN;
    return stack->array[stack->top];
}

int main()
{
    int t;
    scanf("%d",&t);
    while(t-->0)
    {
        int p;
        scanf("%d",&p);
        struct Stack* stack =createStack(p);

        int q;
        scanf("%d",&q);
        int *arr =(int*)malloc(sizeof(int)*q);
        for(int i=0;i<q;i++)
        {
            scanf("%d",arr+i);
        }
        for(int i=0;i<q;i++)
        {
            push(stack,*(arr+i));
        }
        printf("%d popped from stack",pop(stack));

    }
    return 0;
}
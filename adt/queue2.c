/***
 * Implementing an queue using an linked list 
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <limits.h>

struct QueueNode{
    int data;
    struct QueueNode* next;
};

struct Queue{
    struct QueueNode *front, *rear; 
};

struct QueueNode* newNode(int data)
{
    struct QueueNode* queueNode =(struct QueueNode*)malloc(sizeof(struct QueueNode));
    queueNode->data=data;
    queueNode->next=NULL;
    return queueNode;
}

struct Queue* createQueue() 
{ 
    struct Queue* q = (struct Queue*)malloc(sizeof(struct Queue)); 
    q->front = q->rear = NULL; 
    return q; 
} 

int isEmpty(struct QueueNoDE *queue)
{
    return !(queue);
}

void enqueue(struct Queue* q,int data)
{
     
    struct QueueNode* temp = newNode(data); 
  
     
    if (q->rear == NULL) { 
        q->front = q->rear = temp; 
        return; 
    } 
  
     
    q->rear->next = temp; 
    q->rear = temp;     
}

void dequeue(struct Queue* q)
{    
    if (q->front == NULL) 
        return; 
    struct QueueNode* temp = q->front; 
    q->front = q->front->next; 
    if (q->front == NULL) 
        q->rear = NULL; 
    free(temp);

}

/**void topPeek(struct Queue* q)
{
    
}

void bottomPeek(struct Queue* q)
{

}
*/
int main()
{
    int t;
    scanf("%d",&t);
    while(t-->0)
    {
         struct Queue* q = createQueue(); 
         int p;
         scanf("%d",&p);
         int *arr =(int*)malloc(sizeof(int)*p);
         for(int i=0;i<p;i++)
         {
             scanf("%d",arr+i);
         }
         for(int i=0;i<p;i++)
         {
              enQueue(q, *(arr+i)); 
         }
         int p;
         scanf("%d",&p);
         for(int i=0;i<p;i++)
         {
             dequeue(q);
         }
          printf("Queue Front : %d \n", q->front->data); 
            printf("Queue Rear : %d", q->rear->data); 
    }
    return 0;
}

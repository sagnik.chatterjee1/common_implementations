/***
 * Implementing a stack using a linked list 
 * Advantage over using array is that it is dynamic and there
 * is no limit on how many elements that can be inserted
 */

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

struct StackNode{
    int data;
    struct StackNode* next;
};

struct StackNode* newNode (int data)
{
    struct StackNode* stackNode =(struct StackNode*)malloc(sizeof( struct StackNode));
    stackNode->data=data;
    stackNode->next=NULL;
    return stackNode;

}
int isEmpty(struct StackNode* node)
{
    return !node;
}

//no full condition as we can extend it as much as we want

void push(struct StackNode** node,int data)
{
    struct StackNode* stackNode =newNoded(data);
    stackNode->next=*node;
    *node=stackNode;
    printf("%d pushed to stack\n",data);   
}

int pop(struct StackNode** node)
{
    if(isEmpty(*node))
    {
        return INT_MIN;
    }
    struct StackNode* temp =*node;
    *node=(*node)->next;
    int popped =temp->data;
    free(temp);
    return popped;   
}

int peek(struct StackNode* node)
{
    if(isEmpty(node))
    return INT_MIN;
    return node->data;
}

int main()
{
    int t;
    scanf("%d",&t);
    while(t-->0)
    {
        struct StackNode* node =NULL;
        int p;
        scanf("%d",&p);
        int *arr =(int*)malloc(sizeof(int)*p);
        for(int i=0;i<p;i++)
        {
            scanf("%d",arr+i);
        }
        for(int i=0;i<p;i++)
        {
            push(&node,*(arr+i));
        }
        printf("%d popped froms tack \n",pop(&node));
        printf("Top element is %d\n",peek(&node));
    }
    return 0;
}
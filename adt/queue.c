/***
 * Implementing a queue using an array
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <limits.h>

struct Queue{
    int top;
    int bottom;
    unsigned capacity;
    int *arr;
};

struct Queue* createQueue(unsigned capcacity)
{
    struct Queue *queue =(struct Queue*)malloc(sizeof(struct Queue));
    queue->capacity=capcacity;
    queue->bottom=-1;
    queue->top=-1;
    queue->arr=(int*)malloc(sizeof(int)*capcacity);
return queue;

}

bool isFull(struct Queue* queue)
{
    if(queue->top==queue->capacity-1)
    {
        return true;
    }
    return false;
}

bool isEmpty(struct Queue* queue)
{
    if(queue->top==queue->bottom)
    {
        return true;
    }
    return false;
}
//inserting new element into queue ...insertion occurs at bottom position
void enqueue(struct Queue* queue ,int item)
{
    if(isFull(queue))
    {
        printf("Can't enter more elements into queue.Queue Full.\n");
    }
    queue->top=queue->top+1;
    queue->arr[queue->top]=item;
    printf("%d entered into the queue",item);
}

//deleting element from queue ... deletion takes from top position 
int dequeue(struct Queue* queue)
{
    if(isEmpty(queue))
    {
        printf("Can't delete from queue.Queue is empty \n");
        return INT_MIN;
    }
    int temp= queue->arr[queue->top];
    queue->top=queue->top-1;
    printf("%d is removed from the queue",temp);
    return temp;
}

//returns the topmost element in the queue
int peekTop(struct Queue* queue)
{
    if(isEmpty(queue))
    return INT_MIN;
    return queue->arr[queue->top];
}

//returns the topmost eleemnt in the queue
int peekBottom(struct Queue* queue)
{
    if(isEmpty(queue))
    return INT_MIN;
    queue->bottom=queue->bottom+1;
    return queue->arr[queue->bottom];
}
int main()
{
    int t;
    scanf("%d",&t);
    while(t-->0)
    {
        int p;
        scanf("%d",&p);
        struct Queue* queue =createQueue(p);

        int q;
        scanf("%d",&q);
        int *arr =(int*)malloc(sizeof(int)*q);
        for(int i=0;i<q;i++)
        {
            scanf("%d",arr+i);
        }
        for(int i=0;i<q;i++)
        {
            enqueue(queue,*(arr+i));
        }
        printf("%d popped from stack",dequeue(queue));
    printf("%d Top element is \n",peekTop(queue));
    printf("%d Bottom element is \n",peekBottom(queue));
    }
    return 0;
}
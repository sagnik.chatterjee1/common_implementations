'''
Implementing a stack using array /list
'''

from sys import maxsize

##func to create stack 
def createStack():
    stack=[]
    return stack 

##stack is empty when stack size 0
def isEmpty(stack):
    return len(stack)==0

##adding item to stack 
def push(stack,item):
    stack.append(item)
    print(item+" pushed to stack")

##function to remove an item 
def pop(stack):
    if(isEmpty(stack)):
        return str(maxsize-1)
    return stack.pop()

##functo return the top from stack 

def peek(stack):
    if(isEmpty(stack)):
        return str(maxsize-1)
    return stack[len(stack)-1]


def main():
    stack=createStack()
    p=int(input(">"))
    for i in range(0,p):
        p=int(input('>'))
        push(stack,str(p))
    print(pop(stack)+" popped from stack")


if __name__=='__main__':
    main()

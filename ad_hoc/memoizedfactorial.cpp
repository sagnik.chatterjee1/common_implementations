/**
 * Implememnting an memoized solution for the factorial in cpp 
 * using an array to store the intermediate values
 */

#include <bits/stdc++.h>
using namespace std;

#define ll long long 
#define pb push_back

ll int factorial(ll int n)
{
    vector<ll int>arr(n,0);
    arr[0]=1;
    arr[1]=1;
    for(size_t i=0;i<arr.size();i++)
    {
        arr[i]=arr[i-2]+arr[i-1];
    }
    return arr[n];
}

void printVector(vector<ll int>& vec)
{
    for(auto i=vec.begin();i!=vec.end();++i)
    {
        cout << *i<<"\n";
    }
}


int main()
{
    int t;
    cin >> t;
    vector<ll int> arr;
    while(t-->0)
    {
        int p;
        cin >> p;
        arr.pb(factorial(p));
        printVector(arr);
    }
    return 0;
}
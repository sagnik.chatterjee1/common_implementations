/***
 * Implementing an simple facorial using recursion
 */

#include <bits/stdc++.h>
#define ll long long 
#define pb push_back

using namespace std;

int factorial(int n)
{
    if(n==0)
    return 0;
    else if(n==1)
    return 1;
    else return factorial(n-1)*n;
}

void printVector(vector<int>& arr)
{
    for(auto i=arr.begin();i!=arr.end();i++)
    {
        cout << *i <<"\n";
    }
}

int main()
{
    int t;
    cin >> t;
    vector<int> arr;
    while(t-->0)
    {
        int p;
        cin >> p;
        arr.pb(factorial(p));
        printVector(arr);
    }
return 0;
}

#include <stdio.h>
#include <stdlib.h>

struct Node{
    int data ;
    struct Node* left;
    struct Node* right;
};

struct Node* newNode(int data)
{
    struct Node* node =(struct Node*)malloc(sizeof(struct node));
    node->data=data;
    node->left=NULL;
    node->right=NULL;
    return node;
}

struct Node* insert(struct Node* node ,int data,char *arr1,int size2)
{

    if(node==NULL)
    {
        return newNode(data);
    }
    for(int i=0;i<size2;i++)
    {
        if(*(arr1+i)=='l')
        {
            node->left=insert(node->left,data,arr1,size2);
        }
        else if(*(arr1+i)=='r')
        {
            node->right=insert(node->right,data,arr1,size2);
        }
    }
    return node;
}

//printing using inorder traversal 
void inOrder(struct Node* node)
{
    if(node==NULL)
    {
        return ;
    }
    inOrder(node->left);
    printf("%d",node->data);
    inOrder(node->right);
}


int main()
{
    int t;
    scanf("%d",&t);
    while(t-->0)
    {
        int size;
        scanf("%d",&size);
        int *arr =(int*)malloc(sizeof(int)*size);
        for(int i=0;i<size;i++)
        {
            scanf("%d",arr+i);
        }
        int size2=size1-1;
        char *arr1=(char*)malloc(sizeof(char)*(size2));
        for(int i=0;i<(size2);i++)
        {
            //for l and r insertions input in this array 
            scanf("%c",arr1+i);
        }
        struct Node* node = newNode(*(arr+0));//root insertion
        for(int i=1;i<size;i++)
        {
            insert(node,*(arr+i));
        }
        //now printing 
        inOrder(node);

    }
    return 0;
}
#include <stdio.h>
#include <stdlib.h>

struct Node{
    int data ;
    struct Node* left;
    struct Node* right;
};

struct Node* newNode(int data)
{
    struct Node* node =(struct Node*)malloc(sizeof(struct node));
    node->data=data;
    node->left=NULL;
    node->right=NULL;
    return node;
}

struct Node* insertNode(struct Node* node,int data)
{
    if(node==NULL)
    return newNode(key);

    if(data <node->data)
    {
        node->left =insertNode(node->left,data);
    }
    else if(data >node->data)
    {
        node->right=insertNode(node->right,data);
    }
    return node;
}

//inorder traversal for printing 
void inOrder(struct Node *node)
{
    if(node==NULL)
    {
        return ;
    }
    inOrder(node->left);
    printf("%d",node->data);
    inOrder(node->right);
}

int main()
{
    int t;
    scanf("%d",&t);
    while(t-->0)
    {
        int size;
        scanf("%d",&size);
        int *arr =(int*)malloc(sizeof(int)*size);
        for(int i=0;i<size;i++)
        {
            scanf("%d",arr+i);
        }
        struct Node* node = newNode(*(arr+0));//root insertion
        for(int i=1;i<size;i++)
        {
            insert(node,*(arr+i));
        }
        //now printing 
        inOrder(node);

    }
    return 0;
}
